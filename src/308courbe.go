package main

import ("fmt"
	"os"
	"io/ioutil"
	"bitbucket.org/binet/go-gnuplot/pkg/gnuplot"
	"maths/parser"
	"maths/errmng"
	"maths/geom")

func usage() {
	fmt.Fprintln(os.Stderr, "Usage: 308courbe FILE")
	os.Exit(1)
}

func getInc1(n int, xy [](*geom.Point)) (inc1 []float64) {
	inc1 = make([]float64, n + 1)
	
	inc1[0] = geom.NaN
	for i := 1; i < n + 1; i++ {
		inc1[i] = (xy[i].Y() - xy[i - 1].Y()) / (xy[i].X() - xy[i - 1].X())
	}
	return
}

func getInc2(n int, xy [](*geom.Point), inc1 []float64) (inc2 []float64) {
	inc2 = make([]float64, n + 1)
	
	inc2[0] = geom.NaN
	inc2[n] = geom.NaN
	for i := 1; i < n; i++ {
		inc2[i] = (inc1[i + 1] - inc1[i]) / (xy[i + 1].X() - xy[i - 1].X())
	}
	return
}

func initData(n int, xy [](*geom.Point)) (h, inc2 []float64) {
	h = make([]float64, n + 1)
	
	for i, _ := range(xy) {
		if i < 1 {
			h[i] = geom.NaN
		} else {
			h[i] = xy[i].X() - xy[i - 1].X()
		}
	}
	inc2 = getInc2(n, xy, getInc1(n, xy))
	return
}

func makeVector(n int, inc2 []float64) (vt []float64) {
	vt = make([]float64, n)

	for i := 0; i < n; i++ {
		if inc2[i] == geom.NaN {
			vt[i] = 0
		} else {
			vt[i] = 6 * inc2[i]
		}
	}
	return
}

func makeMatrix(n int, h []float64) (mx [][]float64) {
	mx = make([][]float64, n)

	for i := 0; i < n; i++ {
		mx[i] = make([]float64, n)

		if i == 0 {
			mx[i][0] = 1
		} else if i == n - 1 {
			mx[i][n - 1] = 1
		} else {
			mx[i][i - 1] = h[i] / (h[i] + h[i + 1])
			mx[i][i] = 2
			mx[i][i + 1] = h[i + 1] / (h[i] + h[i + 1])
		}
	}
	return
}

func min(t []float64) (min float64) {
	min = t[0]

	for _, v := range t {
		if v < min {
			min = v
		}
	}
	return
}

func max(t []float64) (max float64) {
	max = t[0]

	for _, v := range t {
		if v > max {
			max = v
		}
	}
	return
}

func plotXY(x, y []float64) string {
	f, err := ioutil.TempFile(os.TempDir(), "308courbe")
	if err != nil {
		errmng.Print(err)
		os.Exit(5)
	}
	fname := f.Name()

	for i := 0; i < len(x); i++ {
		f.WriteString(fmt.Sprintf("%v %v\n", x[i], y[i]))
	}

	f.Close()
	return fname
}

func printSpline(n int, xy [](*geom.Point), h, solution []float64, p *gnuplot.Plotter) {
	x, y := make([]float64, n + 1), make([]float64, n + 1)

	for i, v := range xy {
		x[i] = v.X()
		y[i] = v.Y()
	}
	xmin, xmax, ymin, ymax := min(x), max(x), min(y), max(y)
	filename := plotXY(x, y)

	p.CheckedCmd("set parametric")
	p.CheckedCmd("set xrange [%f:%f]", xmin - 1, xmax + 1)
	p.CheckedCmd("set yrange [%f:%f]", ymin - 1, ymax + 1)
	p.CheckedCmd("set trange [%f:%f]", xmin, xmax)
	for i := 1; i <= n; i++ {
		expr := fmt.Sprintf("f%d(t) = ", i)
		expr += fmt.Sprintf("-%f * (((t - %f) ** 3) / (6 * %f))", solution[i - 1], x[i], h[i])
		expr += " + "
		expr += fmt.Sprintf("%f * (((t - %f) ** 3) / (6 * %f))", solution[i], x[i - 1], h[i])
		expr += " - "
		expr += fmt.Sprintf("((%f / %f) - ((%f * %f) / 6)) * (t - %f)", y[i - 1], h[i], h[i], solution[i - 1], x[i])
		expr += " + "
		expr += fmt.Sprintf("((%f / %f) - ((%f * %f) / 6)) * (t - %f)\n", y[i], h[i], h[i], solution[i], x[i - 1])
		p.CheckedCmd(expr)
	}
	expr := "f(t) = "
	for i := 1; i <= n; i++ {
		expr += fmt.Sprintf("t <= %f ? f%d(t) : ", x[i], i)
		if i == n {
			expr += "t"
		}
	}
	expr += "\n"
	p.CheckedCmd(expr)
	expr = fmt.Sprintf("plot \"%s\" title \"Points d'interpolation\" with points, t, f(t) title \"spline\"\n", filename)
	p.CheckedCmd(expr)
}

func main() {
	if len(os.Args) <= 1 {
		usage()
	}
	debug := false
	if len(os.Args) >= 3 && os.Args[2] == "-d" {
		debug = true
	}
	nbLine, xy, err := parser.Parse(os.Args[1])
	if err != nil {
		errmng.Print(err)
		os.Exit(2)
	}
	n := nbLine - 1
	h, inc2 := initData(n, xy)
	if debug {
		geom.PrintVector("h", h)
		geom.PrintVector("inc2", inc2)
		fmt.Printf("n = %d\n", n)
	}
	g := geom.NewGauss(nbLine, makeMatrix(nbLine, h), makeVector(nbLine, inc2))
	g.Debug = debug
	g.FindSolution()
	if debug {
		geom.PrintVector("g.Solution.V", g.Solution.V)
	}
	p, err := gnuplot.NewPlotter("", true, debug)
	if err != nil {
		errS := fmt.Sprintf("Error : %v\n", err)
		fmt.Println(errS)
		os.Exit(4)
	}
	defer p.Close()
	printSpline(n, xy, h, g.Solution.V, p)
}
