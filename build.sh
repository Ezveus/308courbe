#!/bin/sh

PROJ=$(pwd)
SRC=$PROJ/src
PKG=$PROJ/pkg
LIB=$PROJ/lib
export GOPATH=/tmp/go
GOLIB=$GOPATH/pkg
GOSRC=$GOPATH/src
GOPROJ=$GOSRC/maths/308courbe
GOPKG=$GOSRC/maths

echo "Building..."
rm -rf $GOPATH
mkdir -p $GOPROJ
mkdir -p $GOLIB
cp -r $LIB/pkg/* $GOLIB
cp -r $LIB/src/* $GOSRC
cd $GOSRC
cp -r $SRC/* $GOPROJ
cp -r $PKG/* $GOPKG
go build maths/308courbe && cp 308courbe $PROJ && echo "Built" && success=0 || success=1
cd $PROJ
if [ "$success" == 0 ]; then
    rm -rf $GOPATH
fi
exit $success
