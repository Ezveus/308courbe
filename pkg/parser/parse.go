package parser

import ("fmt"
	"os"
	"bufio"
	"maths/geom"
	"maths/conv")

func Parse(filename string) (nbLine int, res [](*geom.Point), err error) {
	nbLine = 0
	res = [](*geom.Point){}
	err = nil

	file, err := os.Open(filename)
	defer file.Close()

	if err != nil {
		return
	}
	data := make([]byte, 1)
	n, err := file.Read(data)
	if n != 1 || err != nil {
		return
	}
	nbLine = conv.CharToInt(data[0])
	file.Read(data)
	res = make([](*geom.Point), nbLine)
	r := bufio.NewReader(file)
	for i := 0; i < nbLine; i++ {
		line, _, _ := r.ReadLine()
		var x,y float64
		fmt.Sscanf(string(line), "%f%f", &x, &y)
		res[i] = geom.NewPoint(x, y)
	}
	return
}
