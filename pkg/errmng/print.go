package errmng

import ("fmt"
	"os")

func Print(err error) error {
	fmt.Fprint(os.Stderr, "Error : ")
	fmt.Fprintln(os.Stderr, err)
	return err
}
