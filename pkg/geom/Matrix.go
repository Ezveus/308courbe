package geom

import "fmt"

var NaN float64 = 9429.4242

type Vector struct {
	Dim int
	V []float64
}

type Matrix struct {
	Row, Col int
	M [][]float64
}

func PrintVector(tabName string, tab []float64) {
	if tab[0] != NaN {
		fmt.Printf("%s : [%g", tabName, tab[0])
	} else {
		fmt.Printf("%s : [NaN", tabName)
	}
	for i, val := range tab {
		if i > 0 {
			if val != NaN {
				fmt.Printf(", %g", val)
			} else {
				fmt.Printf(", NaN")
			}
		}
	}
	fmt.Println("]")
}

func PrintMatrix(tabTabName string, tabTab [][]float64) {
	fmt.Printf("%s : [\n", tabTabName)

	for i, val := range tabTab {
		PrintVector(fmt.Sprintf("%d", i), val)
	}
	fmt.Println("]")
}

func (vt Vector) Set(vector []float64) {
	for i := 0; i < vt.Dim; i++ {
		vt.V[i] = vector[i]
	}
}

func (mx Matrix) Set(matrix [][]float64) {
	for i := 0; i < mx.Row; i++ {
		for j := 0; j < mx.Col; j++ {
			mx.M[i][j] = matrix[i][j]
		}
	}
}

func (vt Vector) Dimensions() int {
	return vt.Dim
}

func (mx Matrix) Dimensions() (int, int) {
	return mx.Row, mx.Col
}

func (vt Vector) AddVector(vt1 Vector) (vtr *Vector, err error) {
	dima, dimb := vt.Dim, vt1.Dim

	if dima == dimb {
		vector := make([]float64, dima)
		err = nil

		for i := 0; i < dima; i++ {
			vector[i] = vt.V[i] + vt1.V[i]
		}
		vtr = NewVector(dima, vector)
	} else {
		vtr = nil
		err = fmt.Errorf("Vectors aren't compatible : %d != %d", dima, dimb)
	}
	return
}

func (vt Vector) NotVector() (vtr *Vector) {
	vector := make([]float64, vtr.Dim)

	for i := 0; i < vt.Dim; i++ {
		vector[i] = vt.V[i] * -1
	}
	vtr = NewVector(vt.Dim, vector)
	return
}

func (vt Vector) SubVector(vt1 Vector) (*Vector, error) {
	return vt.AddVector(*vt1.NotVector())
}

func (mx Matrix) DotMatrix(mx1 Matrix) (mxr *Matrix, err error) {
	rowsa, colsa, rowsb, colsb := mx.Row, mx.Col, mx1.Row, mx1.Col

	if colsa == rowsb {
		matrix := make([][]float64, rowsb)
		err = nil

		for i := 0; i < rowsa; i++ {
			matrix[i] = make([]float64, colsa)
			for j := 0; j < colsb; j++ {
				for k := 0; k < colsa; k++ {
					matrix[i][j] += mx.M[i][k] * mx1.M[k][j]
				}
			}
		}
		mxr = NewMatrix(rowsb, colsa, matrix)
	} else {
		mxr = nil
		err = fmt.Errorf("Matrices aren't compatible : %d != %d", colsa, rowsb)
	}
	return
}

func NewVector(dimension int, vector []float64) (vt *Vector) {
	vt = &Vector{Dim : dimension, V : make([]float64, dimension)}

	vt.Set(vector)
	return
}

func NewMatrix(rows, cols int, matrix [][]float64) (mx *Matrix) {
	mx = &Matrix{Row : rows, Col : cols, M : make([][]float64, rows)}

	for i := 0; i < rows; i++ {
		mx.M[i] = make([]float64, cols)
	}
	mx.Set(matrix)
	return
}

func CopyVector(vt1 Vector) (vt *Vector) {
	vt = &Vector{Dim : vt1.Dim, V : make([]float64, vt1.Dim)}

	vt.Set(vt1.V)
	return
}

func CopyMatrix(mx1 Matrix) (mx *Matrix) {
	mx = &Matrix{Row : mx1.Row, Col : mx1.Col, M : make([][]float64, mx1.Row)}

	for i := 0; i < mx1.Row; i++ {
		mx.M[i] = make([]float64, mx1.Col)
	}
	mx.Set(mx1.M)
	return
}
