package geom

import "fmt"

type Point struct {
	x, y float64
}

func (p Point) To_s() string {
	return fmt.Sprintf("(%f, %f)", p.x, p.y)
}

func (p Point) X() float64 {
	return p.x
}

func (p Point) Y() float64 {
	return p.y
}

func (p Point) setX(x float64) {
	p.x = x
}

func (p Point) setY(y float64) {
	p.y = y
}

func NewPoint(x_, y_ float64) *Point {
	return &Point{x : x_, y : y_}
}

func CopyPoint(p Point) *Point {
	return &Point{x : p.x, y : p.y}
}
