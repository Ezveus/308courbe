package geom

import ("fmt"
	"os"
	"maths/errmng")

type Gauss struct {
	dim int
	Debug bool
	origmatrix, matrix *Matrix
	origvector, vector, Solution *Vector
}

func (g Gauss) GetP(step, line, column int, pivot float64) (p float64) {
	switch {
	case line == column:
		p = 1
	case column == step && line > step:
		p = -1 * g.matrix.M[line][column]
		p /= pivot
	default:
		p = 0
	}
	return
}

func (g Gauss) SwapLines(step int, line *int, line2 int, pivot *float64) {
	if *line >= g.dim {
		errmng.Print(fmt.Errorf("Impossible to find a valid pivot"))
		os.Exit(3)
	}
	if g.matrix.M[*line][0] == 0 {
		if line2 == g.dim {
			*line = 0;
			line2 = *line + 1;
		}
		matrixLine := g.matrix.M[*line];
		component := g.vector.V[*line];
		g.matrix.M[*line] = g.matrix.M[line2];
		g.vector.V[*line] = g.vector.V[line2];
		g.matrix.M[line2] = matrixLine;
		g.vector.V[line2] = component;
	}
	*pivot = g.matrix.M[step][step];
	*line++;
}

func (g Gauss) MatrixMultiplication(step int, pivot float64) {
	matrix := make([][]float64, g.matrix.Row)

	for i := 0; i < g.matrix.Row; i++ {
		matrix[i] = make([]float64, g.matrix.Col)

		for j := 0; j < g.matrix.Col; j++ {
			for k := 0; k < g.matrix.Col; k++ {
				matrix[i][j] += g.GetP(step, i, k, pivot) * g.matrix.M[k][j]
			}
		}
	}
	g.matrix.Set(matrix)
}

func (g Gauss) VectorMultiplication(step int, pivot float64) {
	vector := make([]float64, g.vector.Dim)

	for i := 0; i < g.vector.Dim; i++ {
		for k := 0; k < g.vector.Dim; k++ {
			vector[i] += g.GetP(step, i, k, pivot) * g.vector.V[k]
		}
	}
	g.vector.Set(vector)
}

func (g Gauss) printP(step int, pivot float64, name string) {
	fmt.Println(name)
	for i := 0; i < g.dim; i++ {
		for j := 0; j < g.dim; j++ {
			fmt.Printf("%f | ", g.GetP(step, i, j, pivot))
		}
		fmt.Printf("\n")
	}
	fmt.Println("--------------------------------")
}

func (g Gauss) debug() {
	fmt.Printf("Dimension : %d\n", g.dim)
	PrintMatrix("matrix", g.matrix.M)
	PrintVector("vector", g.vector.V)
}

func (g Gauss) FindSolution() {
	for step, dim := 0, g.dim - 1; step < dim; step++ {
		line := step
		pivot := g.matrix.M[step][step]

		for pivot == 0 {
			g.SwapLines(step, &line, line + 1, &pivot)
		}
		if g.Debug {
			fmt.Printf("Pivot : %g\n", pivot)
			fmt.Printf("Step %d : before multiplication per P\n", step)
			g.printP(step, pivot, "P before mult")
			g.debug();
		}
		g.VectorMultiplication(step, pivot)
		g.MatrixMultiplication(step, pivot)
		if g.Debug {
			fmt.Printf("Step %d : after multiplication per P\n", step)
			g.debug()
		}
		g.Solution.V[dim] = g.vector.V[dim] / g.matrix.M[dim][dim]
		for i := dim; i >= 0; i-- {
			tmp := g.vector.V[i]

			for j := dim; j > i; j-- {
				tmp -= g.matrix.M[i][j] * g.Solution.V[j]
			}
			tmp /= g.matrix.M[i][i]
			g.Solution.V[i] = tmp;
		}
	}
}

func NewGauss(dimension int, matrix [][]float64, vector []float64) *Gauss {
	return &Gauss{dim : dimension, Debug : false, origmatrix : NewMatrix(dimension, dimension, matrix), matrix : NewMatrix(dimension, dimension, matrix), origvector : NewVector(dimension, vector), vector : NewVector(dimension, vector), Solution : NewVector(dimension, make([]float64, dimension))}
}

func CopyGauss(g1 Gauss) *Gauss {
	return &Gauss{dim : g1.dim, Debug : g1.Debug, origmatrix : NewMatrix(g1.origmatrix.Row, g1.origmatrix.Col, g1.origmatrix.M), matrix : NewMatrix(g1.matrix.Row, g1.matrix.Col, g1.matrix.M), origvector : NewVector(g1.origvector.Dim, g1.origvector.V), vector : NewVector(g1.vector.Dim, g1.vector.V), Solution : NewVector(g1.Solution.Dim, g1.Solution.V)}
}
